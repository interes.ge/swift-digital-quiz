-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2021 at 11:09 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `swift-digital-quiz-task`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `is_correct` tinyint(4) NOT NULL,
  `question_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `answer`, `is_correct`, `question_id`) VALUES
(1, 'სავარაუდო პასუხი 1', 0, 1),
(2, 'სავარაუდო პასუხი 2', 0, 1),
(3, 'სწორი პასუხი', 1, 1),
(4, 'სავარაუდო პასუხი 1', 0, 2),
(5, 'სავარაუდო პასუხი 2', 0, 2),
(6, 'სწორი პასუხი', 1, 2),
(7, 'სავარაუდო პასუხი 1', 0, 3),
(8, 'სავარაუდო პასუხი 2', 0, 3),
(9, 'სწორი პასუხი', 1, 3),
(10, 'სავარაუდო პასუხი 1', 0, 4),
(11, 'სავარაუდო პასუხი 2', 0, 4),
(12, 'სწორი პასუხი', 1, 4),
(13, 'სავარაუდო პასუხი 1', 0, 5),
(14, 'სავარაუდო პასუხი 2', 0, 5),
(15, 'სწორი პასუხი', 1, 5),
(16, 'სავარაუდო პასუხი 1', 0, 6),
(17, 'სავარაუდო პასუხი 2', 0, 6),
(18, 'სწორი პასუხი', 1, 6),
(19, 'სავარაუდო პასუხი 1', 0, 7),
(20, 'სავარაუდო პასუხი 2', 0, 7),
(21, 'სწორი პასუხი', 1, 7),
(22, 'სავარაუდო პასუხი 1', 0, 8),
(23, 'სავარაუდო პასუხი 2', 0, 8),
(24, 'სწორი პასუხი', 1, 8),
(25, 'სავარაუდო პასუხი 1', 0, 9),
(26, 'სავარაუდო პასუხი 2', 0, 9),
(27, 'სწორი პასუხი', 1, 9),
(28, 'სავარაუდო პასუხი 1', 0, 10),
(29, 'სავარაუდო პასუხი 2', 0, 10),
(30, 'სწორი პასუხი', 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `difficulity_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `difficulity_id`) VALUES
(1, 'კითხვა 1 <br />\r\nკატეგორია 1<bn />\r\nსირთულე: მარტივი', 1),
(2, 'კითხვა 2 <br />\r\nკატეგორია 1<bn />\r\nსირთულე: მარტივი', 1),
(3, 'კითხვა 3 <br />\r\nკატეგორია 1<bn />\r\nსირთულე: მარტივი', 1),
(4, 'კითხვა 4 <br />\r\nკატეგორია 1<bn />\r\nსირთულე: მარტივი', 1),
(5, 'კითხვა 5 <br />\r\nკატეგორია 1<bn />\r\nსირთულე: მარტივი', 1),
(6, 'კითხვა 6 <br />\r\nკატეგორია 1<bn />\r\nსირთულე: მარტივი', 1),
(7, 'კითხვა 7 <br />\r\nკატეგორია 1<bn />\r\nსირთულე: მარტივი', 1),
(8, 'კითხვა 8 <br />\r\nკატეგორია 1<bn />\r\nსირთულე: მარტივი', 1),
(9, 'კითხვა 9 <br />\r\nკატეგორია 1<bn />\r\nსირთულე: მარტივი', 1),
(10, 'კითხვა 10 <br />\r\nკატეგორია 1<bn />\r\nსირთულე: მარტივი', 1);

-- --------------------------------------------------------

--
-- Table structure for table `questions_link_categories`
--

CREATE TABLE `questions_link_categories` (
  `question` int(11) NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions_link_categories`
--

INSERT INTO `questions_link_categories` (`question`, `category`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `question_categories`
--

CREATE TABLE `question_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question_categories`
--

INSERT INTO `question_categories` (`id`, `title`) VALUES
(1, 'კატეგორია 1'),
(2, 'კატეგორია 2'),
(3, 'კატეგორია 3'),
(4, 'კატეგორია 4'),
(5, 'კატეგორია 5');

-- --------------------------------------------------------

--
-- Table structure for table `question_difficulities`
--

CREATE TABLE `question_difficulities` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question_difficulities`
--

INSERT INTO `question_difficulities` (`id`, `title`) VALUES
(1, 'მარტივი'),
(2, 'საშუალო'),
(3, 'რთული');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_677120094cf6d3f12df0b9dc5d3` (`question_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_2d9822fa76c479d2f8655c3c0c0` (`difficulity_id`);

--
-- Indexes for table `questions_link_categories`
--
ALTER TABLE `questions_link_categories`
  ADD PRIMARY KEY (`question`,`category`),
  ADD KEY `IDX_c03dd24136f629114b24dfda9d` (`question`),
  ADD KEY `IDX_7de7091a1715834ba20dde7d82` (`category`);

--
-- Indexes for table `question_categories`
--
ALTER TABLE `question_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_difficulities`
--
ALTER TABLE `question_difficulities`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `question_categories`
--
ALTER TABLE `question_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `question_difficulities`
--
ALTER TABLE `question_difficulities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `FK_677120094cf6d3f12df0b9dc5d3` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `FK_2d9822fa76c479d2f8655c3c0c0` FOREIGN KEY (`difficulity_id`) REFERENCES `question_difficulities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `questions_link_categories`
--
ALTER TABLE `questions_link_categories`
  ADD CONSTRAINT `FK_7de7091a1715834ba20dde7d82d` FOREIGN KEY (`category`) REFERENCES `question_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_c03dd24136f629114b24dfda9dd` FOREIGN KEY (`question`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
