import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';


const routes: Routes = [
  // {path: '', pathMatch: 'full', redirectTo: 'shell' },
  {
    path: '',
    loadChildren: () => import(`./routes/quiz-select/quiz-select.module`).then(m => m.QuizSelectModule)
  },
  {
    path: 'quiz',
    loadChildren: () => import(`./routes/quiz/quiz.module`).then(m => m.QuizModule)
  },
  {
    path: 'result',
    loadChildren: () => import(`./routes/quiz-result/quiz-result.module`).then(m => m.QuizResultModule)
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
