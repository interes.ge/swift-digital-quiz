
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {TableModule} from 'primeng/table';
import {MultiSelectModule} from 'primeng/multiselect';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import {FileUploadModule} from 'primeng/fileupload';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {TooltipModule} from 'primeng/tooltip';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ToastModule} from 'primeng/toast';
import {CheckboxModule} from 'primeng/checkbox';
import {InputSwitchModule} from 'primeng/inputswitch';
import {DialogModule} from 'primeng/dialog';
import {PanelModule} from 'primeng/panel';
import {AccordionModule} from 'primeng/accordion';
import {BlockUIModule} from 'primeng/blockui';
import {TreeModule} from 'primeng/tree';
import {DropdownModule} from 'primeng/dropdown';
import { ProgressBarModule } from 'primeng/progressbar';




export const PRIME_NG_MODULES: any[] = [
  ButtonModule,
  InputTextModule,
  TableModule,
  MultiSelectModule,
  InputTextareaModule,
  EditorModule,
  FileUploadModule,
  MessagesModule,
  MessageModule,
  TooltipModule,
  AutoCompleteModule,
  ToastModule,
  CheckboxModule,
  InputSwitchModule,
  DialogModule,
  PanelModule,
  AccordionModule,
  BlockUIModule,
  TreeModule,
  DropdownModule,
  ProgressBarModule
];
