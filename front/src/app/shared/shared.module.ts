import { NgModule } from '@angular/core';
import {PRIME_NG_MODULES} from './primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpWrapperService } from './services/http-wrapper.service';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { QuizService } from './services/quiz.service';


@NgModule({
  declarations: [],
  providers: [ HttpWrapperService, QuizService ],
  exports: [
    PRIME_NG_MODULES,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  imports: [
    PRIME_NG_MODULES,
    HttpClientModule,
    RouterModule
  ]
})
export class SharedModule { }
