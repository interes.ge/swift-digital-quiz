export abstract class AbstractModel {

  protected constructor(options = {}) {
    this.storeValues(options);
  }

  storeValues(options: any = {}) {
    options = this.toCamel(options);
    for (const propertyName in options) {
      if (options.hasOwnProperty(propertyName) && typeof options[propertyName] !== 'undefined') {
        this[propertyName] = options[propertyName];
      }
    }
  }

  toCamel(obj) {
    const newObj = {};
    for (const propertyName in obj) {
      if (obj.hasOwnProperty(propertyName) && typeof obj[propertyName] !== 'undefined') {
        const propertyNameCamelCase = propertyName.replace(/_([a-z])/g, (m, firstChar) => {
          return firstChar.toUpperCase();
        });
        newObj[propertyNameCamelCase] = obj[propertyName];
        if (typeof newObj[propertyNameCamelCase] === 'object' && newObj[propertyNameCamelCase] !== null) {
          this.toCamel(newObj[propertyNameCamelCase]);
        }

        if (Array.isArray(newObj[propertyNameCamelCase]) && newObj[propertyNameCamelCase].length > 0) {
          for (let i = 0; i < newObj[propertyNameCamelCase].length; i++) {
            if (typeof newObj[propertyNameCamelCase][i] === 'object' && newObj[propertyNameCamelCase][i] !== null) {
              newObj[propertyNameCamelCase][i] = this.toCamel(newObj[propertyNameCamelCase][i]);
            }
          }
        }

      }
    }
    return newObj;
  }
}

