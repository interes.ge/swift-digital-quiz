import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpWrapperService } from 'src/app/shared/services/http-wrapper.service';
import {  Observable } from 'rxjs';



@Injectable()
export class QuizService {
  constructor(private httpWrapperService: HttpWrapperService) { }

  listCategories(): Observable<any> {
    return this.httpWrapperService.get(`http://localhost:3000/question-categories`);
  };  


  listDifficulities(): Observable<any> {
    return this.httpWrapperService.get(`http://localhost:3000/question-difficulities`);
  }; 


  getQuiz(requestData: any): Observable<any> {
    return this.httpWrapperService.post(`http://localhost:3000/questions/get-quiz`, requestData);
  }; 

}