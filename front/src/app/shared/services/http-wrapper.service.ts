import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class HttpWrapperService {

    constructor(private http: HttpClient) {}

    get(apiUrl: string, options: any = {}) {
        const header = new HttpHeaders({'content-type': 'application/json'});
        options.header = header;
        return this.http.get(apiUrl, options);
    }

    post(apiUrl: string, parameters: any = {}, options: any = {}) {
        // const header = new HttpHeaders({'content-type': 'application/json'});
        // options.header = header;
        return this.http.post(apiUrl, parameters, options);
    }

    postFile(apiUrl: string, parameters: any = {}, options: any = {}) {
        return this.http.post(apiUrl, parameters, options);
    }
}
