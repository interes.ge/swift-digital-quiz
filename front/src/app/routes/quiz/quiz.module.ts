import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizComponent } from './quiz.component';
import { QuizRoutingModule } from './quiz-routing.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { quizSelectReducer } from '../state/quiz-select.reducer';
import { QuizSelectEffects } from '../state/quiz-select.effects';



@NgModule({
  declarations: [QuizComponent],
  imports: [
    CommonModule,
    QuizRoutingModule,
    StoreModule.forFeature('quizState', quizSelectReducer),
    EffectsModule.forFeature([QuizSelectEffects])
  ]
})
export class QuizModule { }
