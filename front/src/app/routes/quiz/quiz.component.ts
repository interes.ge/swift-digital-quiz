import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Question, State } from '../state/quiz-select-state.model';
import { selectQuizData, selectRequestedQuizInfo } from '../state/quiz-select.selectors';
import * as QuizActions from '../state/quiz-select.actions';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit, OnDestroy {
  private destroy$: Subject<boolean> = new Subject<boolean>();
  quizInfo: {categoryId: number, difficulityId: number}
  questions: Question[]
  
  constructor(
    private store: Store<State>,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.subscribeQuizData()
    this.getRequestedQuizInfo()
  }

  subscribeQuizData(){
    this.store.select(selectQuizData).pipe(takeUntil(this.destroy$)).subscribe(quiz => {
      this.questions = quiz
      if(!this.questions.length)this.getRequestedQuizInfo()
    })
  }

  getRequestedQuizInfo(){
    this.store.select(selectRequestedQuizInfo).pipe(takeUntil(this.destroy$)).subscribe(quizInfo => {
      this.quizInfo = quizInfo
      if(quizInfo)this.getQuiz(quizInfo)
      else{
        this.router.navigate([''])
      }

    })
  }

  getQuiz(requestData){
    this.store.dispatch(QuizActions.getQuiz(requestData))
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
