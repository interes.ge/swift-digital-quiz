import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizSelectComponent } from './quiz-select.component';
import { QuizSelectRoutingModule } from './quiz-select-routing.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { quizSelectReducer } from '../state/quiz-select.reducer';
import { QuizSelectEffects } from '../state/quiz-select.effects';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [QuizSelectComponent],
  imports: [
    CommonModule,
    SharedModule,
    QuizSelectRoutingModule,
    StoreModule.forFeature('quizState', quizSelectReducer),
    EffectsModule.forFeature([QuizSelectEffects])
  ]
})
export class QuizSelectModule { }
