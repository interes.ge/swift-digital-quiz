import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { QuestionCategories, QuestionDifficulities, State } from '../state/quiz-select-state.model';
import * as QuizSelectActions from '../state/quiz-select.actions';
import { selectCategoryList, selectDifficulityList } from '../state/quiz-select.selectors';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';



@Component({
  selector: 'app-quiz-select',
  templateUrl: './quiz-select.component.html',
  styleUrls: ['./quiz-select.component.scss']
})
export class QuizSelectComponent implements OnInit, OnDestroy {
  private destroy$: Subject<boolean> = new Subject<boolean>();

  categories: QuestionCategories[]
  difficulities: QuestionDifficulities[]

  quizSelectForm: FormGroup
  
  constructor(
    private store: Store<State>,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.subscribeCategoryList()
    this.subscribeDifficulityList()
    this.initForm()
  }

  initForm(){
    this.quizSelectForm = new FormGroup({
      'categoryId': new FormControl(null, { validators: [Validators.required] }),
      'difficulityId': new FormControl(null, { validators: [Validators.required] })
    })
  }

  subscribeCategoryList(){
    this.store.select(selectCategoryList).pipe(takeUntil(this.destroy$)).subscribe(categories => {
      this.categories = categories
      if(!categories.length){
        this.loadCategories()
      }

    })
  }

  loadCategories(){
    this.store.dispatch(QuizSelectActions.loadCategories())
  }

  subscribeDifficulityList(){
    this.store.select(selectDifficulityList).pipe(takeUntil(this.destroy$)).subscribe(difficulities => {
      this.difficulities = difficulities
      if(!difficulities.length){
        this.loadDifficulities()
      }

    })
  }

  loadDifficulities(){
    this.store.dispatch(QuizSelectActions.loadDifficulities())
  }

  getQuiz(){
    console.log(this.quizSelectForm.value)
    if(this.quizSelectForm.valid){
      this.store.dispatch(QuizSelectActions.setRequiredQuiz(this.quizSelectForm.value))
      this.router.navigate(['quiz'])
    }
    
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
