import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuizSelectComponent } from './quiz-select.component';



const routes: Routes = [
  {
    path: '', component: QuizSelectComponent,
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuizSelectRoutingModule { }
