import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizResultComponent } from './quiz-result.component';
import { QuizResultRoutingModule } from './quiz-result-routing.module';



@NgModule({
  declarations: [QuizResultComponent],
  imports: [
    CommonModule,
    QuizResultRoutingModule
  ]
})
export class QuizResultModule { }
