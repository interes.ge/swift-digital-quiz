import { createReducer, on, createFeatureSelector, createSelector } from '@ngrx/store';
import * as QuizActions from './quiz-select.actions';
import { QuizSelectState } from './quiz-select-state.model';



const initialState: QuizSelectState = {
    selectedCategory: null,
    selectedDifficulity: null,
    questionCategories: [],
    questionDifficulities: [],
    quiz: []
};


export const quizSelectReducer = createReducer<QuizSelectState>(
    initialState,

    on(QuizActions.setCategories, (state, action): QuizSelectState => {
        return {
            ...state,
            questionCategories: action.categories,
        }
    }),

    on(QuizActions.setDifficulities, (state, action): QuizSelectState => {
        return {
            ...state,
            questionDifficulities: action.difficulities,
        }
    }),

    on(QuizActions.setRequiredQuiz, (state, action): QuizSelectState => {
        return {
            ...state,
            selectedCategory: action.categoryId,
            selectedDifficulity: action.difficulityId,
        }
    }),

    on(QuizActions.setQuizData, (state, action): QuizSelectState => {
        return {
            ...state,
            quiz: action.questions
        }
    }),

    

    

);