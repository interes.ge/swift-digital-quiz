import * as AppState from '../../state/app.state';

export class State extends AppState.State {
    quizSelectState: QuizSelectState;
}

export class QuizSelectState { //extends AppState.State
    selectedCategory: number
    selectedDifficulity: number
    questionCategories:  QuestionCategories[]
    questionDifficulities:  QuestionDifficulities[]
    quiz: Question[]
}

export class QuestionCategories{
    id: number
    title: string;
}

export class QuestionDifficulities{
    id: number
    title: string;
}

export class Question{
    question: string
    answers: Answer[];
}

export class Answer{
    id: number;
    answer: string
}