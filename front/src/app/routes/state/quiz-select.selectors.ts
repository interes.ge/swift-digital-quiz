import { createFeatureSelector, createSelector } from "@ngrx/store";
import { QuizSelectState } from "./quiz-select-state.model";

const getQuizSelectFeatureState = createFeatureSelector<QuizSelectState>('quizState');

export const selectCategoryList = createSelector(
    getQuizSelectFeatureState,
    state => state && state.questionCategories ? state.questionCategories : []
);

export const selectDifficulityList = createSelector(
    getQuizSelectFeatureState,
    state => state && state.questionDifficulities ? state.questionDifficulities : []
);

export const selectSelectedCategory = createSelector(
    getQuizSelectFeatureState,
    state => state && state.selectedCategory ? state.selectedCategory : null
);

export const selectSelectedDifficulity = createSelector(
    getQuizSelectFeatureState,
    state => state && state.selectedDifficulity ? state.selectedDifficulity : null
);

export const selectRequestedQuizInfo = createSelector(
    selectSelectedCategory,
    selectSelectedDifficulity,
    (categoryId, difficulityId) => categoryId && difficulityId ? {categoryId: categoryId, difficulityId:categoryId} : null
);

export const selectQuizData = createSelector(
    getQuizSelectFeatureState,
    state => state && state.quiz ? state.quiz : []
);