import { createAction, props } from '@ngrx/store';
import { Question, QuestionCategories, QuestionDifficulities, } from './quiz-select-state.model';

export const loadCategories = createAction(
    '[Quiz Select Component] load categories',
);

export const setCategories = createAction(
    '[Quiz Select Component] set categories',
    props<{ categories: QuestionCategories[] }>()
);


export const loadDifficulities = createAction(
    '[Quiz Select Component] load difficulities',
);


export const setDifficulities = createAction(
    '[Quiz Select Component] set difficulities',
    props<{ difficulities: QuestionDifficulities[] }>()
);

export const setRequiredQuiz = createAction(
    '[Quiz Select Component] set required quiz specifications',
    props<{ categoryId: number, difficulityId: number }>()
);

export const getQuiz = createAction(
    '[Quiz Page Component] get quiz',
    props<{ categoryId: number, difficulityId: number }>()
);

export const setQuizData = createAction(
    '[Quiz Select Component] set difficulities',
    props<{ questions: Question[] }>()
);


