import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as QuizSelectActions from './quiz-select.actions';
import { mergeMap, map } from 'rxjs/operators';
import { QuizService } from "../../shared/services/quiz.service";
@Injectable()
export class QuizSelectEffects {
    constructor(
        private actions$: Actions,
        private quizService: QuizService
        ) { }

        loadCategories$ = createEffect(() => {
            return this.actions$.pipe(
                ofType(QuizSelectActions.loadCategories),
                    mergeMap(() => this.quizService.listCategories().pipe(
                    map(res => QuizSelectActions.setCategories({ categories: res }))
                    )
                )
            )
        })

        loadDifficulities$ = createEffect(() => {
            return this.actions$.pipe(
                ofType(QuizSelectActions.loadDifficulities),
                    mergeMap(() => this.quizService.listDifficulities().pipe(
                    map(res => QuizSelectActions.setDifficulities({ difficulities: res }))
                    )
                )
            )
        })


        getQuiz$ = createEffect(() => {
            return this.actions$.pipe(
                ofType(QuizSelectActions.getQuiz),
                    mergeMap((requestData) => this.quizService.getQuiz(requestData).pipe(
                    map(res => QuizSelectActions.setQuizData({ questions: res }))
                    )
                )
            )
        })

        

        
    

    
}