import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { QuestionDifficulities } from "src/database/entity/difficulities.entity";
import { DifficulitiesController } from "./difficulities.controller";
import { DifficulitiesService } from "./difficulities.service";


@Module({
    imports: [TypeOrmModule.forFeature([QuestionDifficulities])],
    controllers: [DifficulitiesController],
    providers: [DifficulitiesService]
})
export class DifficulitiesModule{

}