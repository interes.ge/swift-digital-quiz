import { Controller, Get } from "@nestjs/common";
import { DifficulitiesService } from "./difficulities.service";

@Controller('question-difficulities')
export class DifficulitiesController {
    constructor(
        private readonly difficulitiesService: DifficulitiesService
        ){}

    @Get()
    async listQuestionDifficulities(): Promise<any>{
        return await this.difficulitiesService.listQuestionDifficulities()
    }
    
}