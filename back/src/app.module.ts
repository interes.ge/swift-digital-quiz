import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { DifficulitiesModule } from './difficulities/difficulities.module';
import { QuestionCategoriesModule } from './question-categories/question-categories.module';
import { QuestionsModule } from './questions/question.module';

@Module({
  imports: [
    DatabaseModule,
    QuestionCategoriesModule,
    QuestionsModule,
    DifficulitiesModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
