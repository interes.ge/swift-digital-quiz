import { Body, Controller, Get, Post } from "@nestjs/common";
import { QuestionsService } from "./question.service";

class QuizRequestInterface {
    categoryId: number
    difficulityId: number
    questionCount: number
}

@Controller('questions')
export class questionCategoriesController {
    constructor(
        private readonly questionsService: QuestionsService
        ){}

    @Post('get-quiz')
    async getQuiz(@Body() quizRequest: QuizRequestInterface ): Promise<any>{
        return await this.questionsService.getQuestionsByCategoryId(quizRequest.categoryId, quizRequest.difficulityId, quizRequest.questionCount)
    }
    
}