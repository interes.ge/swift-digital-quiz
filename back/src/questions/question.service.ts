import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Answers } from "src/database/entity/answers.entity";
import { QuestionCategories } from "src/database/entity/question-categories.entity";
import { Questions } from "src/database/entity/questions.entity";
import { getManager, Repository } from "typeorm";




@Injectable()
export class QuestionsService{
    private entityManager

    constructor(
        @InjectRepository(Questions) private questionsRepository: Repository<Questions>,
        @InjectRepository(Answers) private answersRepository: Repository<Answers>,
        // @InjectRepository(Reviews) private reviewsRepository: Repository<Reviews>
        ){
        this.entityManager = getManager();
    }


    async getQuestionsByCategoryId(categoryId: number, difficulityId: number, questionCount: number = 5 ):Promise<any[]>{
        return new Promise(async (resolve, reject) => {
            const query = `
            SELECT
            questions.id,
            questions.question
            FROM questions
            LEFT JOIN questions_link_categories ON questions_link_categories.question = questions.id
            WHERE
            questions_link_categories.category = ?
            AND
            questions.difficulity_id = ?
            ORDER BY RAND()
            LIMIT ?
            `;            
            
            const questionsPromise = this.entityManager.query(query , [categoryId, difficulityId, questionCount])

            questionsPromise.then(async questions => {
                const answerPromises = []
                questions.forEach(question => {
                    const answerPromise = this.entityManager.query(`SELECT answers.answer, answers.id FROM answers WHERE question_id = ? ORDER BY RAND()` , [question.id])
                    answerPromises.push(answerPromise)
                });

                Promise.all(answerPromises).then(res => {
                    for(let i = 0; i< res.length; i++){
                        questions[i].answers = res[i]
                        delete questions[i].id
                    }
                    resolve(questions)
                })
                .catch(err => reject(err))
                }
            ).catch(err => reject(err))

        });
    }
    
}