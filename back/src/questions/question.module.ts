import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Answers } from "src/database/entity/answers.entity";
import { Questions } from "src/database/entity/questions.entity";
import { questionCategoriesController } from "./question.controller";
import { QuestionsService } from "./question.service";


@Module({
    imports: [TypeOrmModule.forFeature([Questions, Answers])],
    controllers: [questionCategoriesController],
    providers: [QuestionsService]
})
export class QuestionsModule{

}