import { Injectable } from "@nestjs/common";



@Injectable()
export class AnswersService{
    private entityManager

    constructor(
        // @InjectRepository(Courses) private coursesRepository: Repository<Courses>,
        // @InjectRepository(CourseModules) private modulesRepository: Repository<CourseModules>,
        // @InjectRepository(Reviews) private reviewsRepository: Repository<Reviews>
        ){
        // this.entityManager = getManager();
    }




    // async listCourses(requestData: ArticleListParam):Promise<any[]>{
    //     return new Promise(async (resolve, reject) => {
    //         const slugQuery = requestData.slug && requestData.slug!='კურსები' ? ` AND article_categories.slug = '${requestData.slug}'` : '';
    //         const query = `
    //         SELECT
    //         courses.id
    //         FROM courses
    //         LEFT JOIN courses_link_categories ON courses_link_categories.course = courses.id
    //         LEFT JOIN course_categories ON courses_link_categories.category = course_categories.id
    //         WHERE
    //         courses.id NOT IN (?)
    //         ${slugQuery}
    //         ORDER BY ? ?
    //         LIMIT ?, ?
    //         `;

    //         // console.log(query)

    //         const courseIdsPromise = this.entityManager.query(query , [requestData.excludeIds, requestData.order.column, requestData.order.direction, requestData.limit.start, requestData.limit.size])

    //         courseIdsPromise.then(courseIds => {
    //             courseIds = courseIds.map(a=>a.id);
    //             // console.log(courseIds)
    //                 const articlesPromise = this.coursesRepository.createQueryBuilder("courses")
    //                 .select("courses.id")
    //                 .addSelect("courses.slug")
    //                 .addSelect("courses.title")
    //                 .addSelect("courses.imgUrl")
    //                 .addSelect("courses.description")
    //                 .leftJoinAndSelect("courses.categories", "categories")
    //                 .leftJoinAndSelect("courses.authors", "authors")
    //                 .where("courses.id IN (:id)", { id: courseIds })
    //                 .orderBy(`courses.${requestData.order.column}`, requestData.order.direction)
    //                 .getMany()

    //                 // resolve(courseIds)

    //                 articlesPromise.then(articles => resolve(articles))
    //                 .catch(err => reject(err))
    //             }
    //         ).catch(err => reject(err))

    //     });
    // }

    // async getCourseBySlug(slug:string){
    //     return new Promise(async (resolve, reject) => {
    //         this.coursesRepository.createQueryBuilder("courses")
    //         .leftJoinAndSelect("courses.categories", "categories")
    //         .leftJoinAndSelect("courses.authors", "authors")
    //         .where("courses.slug = (:slug)", { slug: slug })
    //         .getOne()
    //         .then(async res => {
    //             // const reviewSummery = {rating: 4.5, reviewCount: 2};
    //             // const course = {...res, reviewSummery, tableOfContent: await this.getTableOfContent(res.id)};
    //             resolve(res)
    //         }).catch(err => reject(err))
            
    //         // this.articlesRepository.findOne({slug: slug}).then(res => resolve(res)).catch(err => reject(err))
    //     })
    // }

    // async getModuleContent(parentId:number):Promise<any>{
    //     return new Promise(async (resolve, reject) => {
    //         this.modulesRepository.createQueryBuilder("course_modules")
    //         .leftJoinAndSelect("course_modules.modules", "modules")
    //         .leftJoinAndSelect("course_modules.lessons", "lessons")
    //         .where("course_modules.parent_id = (:parentId)", { parentId: parentId })
    //         .getMany()
    //         .then(result => {
    //             const promises = [];
    //             result.forEach(async module => {
    //                 promises.push(this.getModuleContent(module.id))
    //             })
    //             Promise.all(promises).then(res => {
    //                 for(let i=0; i<res.length; i++){
    //                     result[i] = {...result[i], modules: res[i]}
    //                 }
    //                 resolve(result)
    //             })
    //             .catch(err => reject(err))
    //         }).catch(err => reject(err))
            
    //         // this.articlesRepository.findOne({slug: slug}).then(res => resolve(res)).catch(err => reject(err))
    //     })
    // }



    // async getTableOfContent(courseId:number){
    //     return new Promise(async (resolve, reject) => {
    //         this.modulesRepository.createQueryBuilder("course_modules")
    //         // .leftJoinAndSelect("course_modules.modules", "modules")
    //         .leftJoinAndSelect("course_modules.lessons", "lessons")
    //         .where("course_modules.parent_id is null")
    //         .getMany()
    //         .then(result => {
    //             const promises = [];
    //             result.forEach(async module => {
    //                 promises.push(this.getModuleContent(module.id))
    //             })
    //             Promise.all(promises).then(res => {
    //                 for(let i=0; i<res.length; i++){
    //                     result[i] = {...result[i], modules: res[i]}
    //                 }
    //                 resolve(result)
    //             })
    //             .catch(err => reject(err))
                
    //         }).catch(err => reject(err))
            
    //         // this.articlesRepository.findOne({slug: slug}).then(res => resolve(res)).catch(err => reject(err))
    //     })
    // }

    // async getReviewStatisticsAndListOfReviews(courseId:number){
    //     console.log('review-statistics')
    //     return new Promise(async (resolve, reject) => {
    //         const promises = [];
    //         promises.push(this.getReviewStatistics(courseId));
    //         promises.push(this.listReviews(courseId, null, 0, 4));
    //         Promise.all(promises).then(results =>{
    //             const totalPoints = results[0].reduce( (points, ratingStat) => points + ratingStat.rating * ratingStat.votes, 0 );
    //             const totalReviews = results[0].reduce( (votes, ratingStat) => parseInt(votes) + parseInt(ratingStat.votes), 0 );
    //             resolve(
    //                 {
    //                     average: Math.round(totalPoints / totalReviews * 10) / 10,
    //                     reviewCount: totalReviews,
    //                     details: results[0],
    //                     reviewList: results[1]
    //                 }
    //             ) 
    //         })
    //     })
    // }


    // async getReviewStatistics(courseId:number){
    //     return new Promise(async (resolve, reject) => {
    //         this.entityManager.query(`SELECT rating, COUNT(rating) AS votes FROM reviews WHERE course_id = ? GROUP BY rating ORDER BY rating ASC`, [courseId])
    //         .then(result => {
    //             resolve(result)
    //         }).catch(err => reject(err))
            
    //         // this.articlesRepository.findOne({slug: slug}).then(res => resolve(res)).catch(err => reject(err))
    //     })
    // }

    // async listReviews(courseId:number, rating: number, start: number, size: number ){
    //     console.log(courseId, rating, start, size)
    //     return new Promise(async (resolve, reject) => {
    //         // const reviewListPromise = this.reviewsRepository.createQueryBuilder("reviews")
    //         //         .select("reviews.id")
    //         //         .where("courses.rating  = :id ", { id: courseId })
    //         //         .orderBy(`courses.${requestData.order.column}`, requestData.order.direction)
    //         //         .getMany()

    //         const ratingQuery = rating ? ` AND rating = ${rating}`: '';

    //         this.entityManager.query(`SELECT id FROM reviews WHERE course_id = ? ${ratingQuery} LIMIT ?, ? `, [courseId, start, size])
    //         .then(result => {
    //             if(result.length){
    //                 const reviewListPromise = this.reviewsRepository.createQueryBuilder("reviews")
    //                 .select("reviews.id")
    //                 .addSelect("reviews.rating")
    //                 .addSelect("reviews.message")
    //                 .addSelect("reviews.addTimestamp")
    //                 .leftJoinAndSelect("reviews.author", "author")
    //                 .where("reviews.id IN (:ids)", { ids: result.map(review=>review.id) })
    //                 .getMany()
                
    //                 reviewListPromise.then(reviews => resolve(reviews)).catch(err => reject(err))
    //             }
    //             else resolve([])

    //         }).catch(err => reject(err))
            
    //         // this.articlesRepository.findOne({slug: slug}).then(res => resolve(res)).catch(err => reject(err))
    //     })
    // }
    
}