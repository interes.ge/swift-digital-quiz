import { Controller, Get } from "@nestjs/common";
import { AnswersService } from "./answers.service";

@Controller('answers')
export class AnswersController {
    constructor(
        private readonly answersService: AnswersService
        ){}

    @Get()
    async getHello(): Promise<any>{
        return "Hello World!!!"
    }

    // @Get(':slug')
    // async getArticleBySlug(@Param('slug') slug: string): Promise<any>{
    //     return await this.courseService.getCourseBySlug(slug)
    // }

    // @Post('list')
    // async listCourses(@Body() requestBody: ArticleListParam ){
    //     // console.log(requestBody)
    //     return {objectList: await this.courseService.listCourses(requestBody) }
    // }

    // @Get(':courseId/table-of-content')
    // async listModules(@Param('courseId') courseId: number){
    //     console.log(courseId)
    //     return await this.courseService.getTableOfContent(courseId)
    // }

    // @Get(':courseId/review-statistics')
    // async getCourseReviewStatisticsAndListOfReviews(@Param('courseId') courseId: number){
    //     console.log('review-statistics')
    //     return await this.courseService.getReviewStatisticsAndListOfReviews(courseId)
    // }

    // @Post('reviews')
    // async listCourseReviews(@Body() requestBody: any ){
    //     return await this.courseService.listReviews(requestBody.courseId, requestBody.rating, (requestBody.page - 1) * 4 , 4)
    // }
    
}