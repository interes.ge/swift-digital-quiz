import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { QuestionCategories } from "src/database/entity/question-categories.entity";
import { questionCategoriesController } from "./question-categories.controller";
import { QuestionCategoriesService } from "./question-categories.service";


@Module({
    imports: [TypeOrmModule.forFeature([QuestionCategories])],
    controllers: [questionCategoriesController],
    providers: [QuestionCategoriesService]
})
export class QuestionCategoriesModule{

}