import { Controller, Get } from "@nestjs/common";
import { QuestionCategoriesService } from "./question-categories.service";

@Controller('question-categories')
export class questionCategoriesController {
    constructor(
        private readonly questionCategoriesService: QuestionCategoriesService
        ){}

    @Get()
    async listQuestionCategories(): Promise<any>{
        return await this.questionCategoriesService.listQuestionCategories()
    }


    
}