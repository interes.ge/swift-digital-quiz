import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Questions } from "./questions.entity";

@Entity()
export class QuestionCategories {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    title: string;
  
    @ManyToMany(() => Questions, question => question.categories)
    questions: Questions[];
    
}