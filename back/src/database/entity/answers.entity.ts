import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { QuestionCategories } from "./question-categories.entity";
import { Questions } from "./questions.entity";

@Entity()
export class Answers {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    answer: string;

    @Column()
    isCorrect: boolean;

    @ManyToOne(() => Questions, question => question.answers)
    question: Questions;

}