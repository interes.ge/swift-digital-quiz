import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Answers } from "./answers.entity";
import { QuestionDifficulities } from "./difficulities.entity";
import { QuestionCategories } from "./question-categories.entity";

@Entity()
export class Questions {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    question: string;
  
    @ManyToMany(() => QuestionCategories, category => category.questions)
    @JoinTable(
        {
            name: "questions_link_categories",
            joinColumn: {
                name: "question",
                referencedColumnName: "id"
            },
            inverseJoinColumn: {
                name: "category",
                referencedColumnName: "id"
            }
        }
    )
    categories: QuestionCategories[];


    @OneToMany(type => Answers, answer=> answer.id)
    answers: Answers[];

    @ManyToOne(() => QuestionDifficulities, difficulity => difficulity.questions)
    difficulity: QuestionDifficulities;
    
}