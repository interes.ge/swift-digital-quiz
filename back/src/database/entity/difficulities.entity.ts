import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Answers } from "./answers.entity";
import { QuestionCategories } from "./question-categories.entity";
import { Questions } from "./questions.entity";

@Entity()
export class QuestionDifficulities {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    title: string;


    @OneToMany(type => Questions, question=> question.difficulity)
    questions: Questions[];
    
}