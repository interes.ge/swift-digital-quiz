import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { SnakeNamingStrategy } from "typeorm-naming-strategies/snake-naming.strategy";
import { Answers } from "./entity/answers.entity";
import { QuestionDifficulities } from "./entity/difficulities.entity";
import { QuestionCategories } from "./entity/question-categories.entity";
import { Questions } from "./entity/questions.entity";


@Module({
    imports: [
      TypeOrmModule.forRoot({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: '',
        database: 'swift-digital-quiz-task',
        entities: [
          // Users,
          // RefreshTokens,
          // ArticleCategories,
          // Articles,
          // PageLayout,
          // PageBlock,
          // Keywords,
          // CourseCategories,
          // Courses,
          // CourseModules,
          // Lessons,
          // Reviews,
          QuestionCategories,
          Questions,
          Answers,
          QuestionDifficulities
        ],  
        namingStrategy: new SnakeNamingStrategy(),
        synchronize: true, 
      })
    ], 
  })
  export class DatabaseModule {}